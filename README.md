# A Raffle Lottery Built On Ethereum

This is an example of a raffle or lottery that can be run as a smart contract on a blockchain such as Ethereuem. This is only run on the Ethereum Rinkeby test network and is for demostration purposes only.

[Enter the lottery here](https://ethereum-react-raffle-lottery.herokuapp.com/)

To use the functionality you will need to have the [MetaMask browser](https://www.google.com/search?q=metamask+extension) installed. You can get some Rinkeby test ETH at any Rinkeby faucet such as [this one](https://faucets.chain.link/rinkeby).

## Created by Richard Kronick

[See my portfolio](https://www.richardkronick.com/)
